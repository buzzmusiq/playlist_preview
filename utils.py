import numpy as np
import os
import string
import librosa


def print_hi():
    print(' _   _ _     ____                                    _       _')
    print('| | | (_)   | __ ) _   _ _________ __ ___  _   _ ___(_) __ _| |')
    print("| |_| | |   |  _ \| | | |_  /_  / '_ ` _ \| | | / __| |/ _` | |")
    print('|  _  | |_  | |_) | |_| |/ / / /| | | | | | |_| \__ \ | (_| |_|')
    print('|_| |_|_( ) |____/ \__,_/___/___|_| |_| |_|\__,_|___/_|\__, (_)')
    print('        |/                                                |_| ')


def assert_type(correct_type, attr, attr_name, positive=False):
    assert isinstance(attr, correct_type), \
        'Wrong type of {}, {}'.format(attr_name, type(attr))
    if positive:
        assert_positive(attr, attr_name)


def stereo_concatenate(src1, src2):
    """force stereo concat"""
    if src1.ndim == 1 or src.shape[0] == 1:
        src1 = np.atleast_2d(src1)
        src1 = np.tile(src1, [2, 1])
        # TODO
        # Finish it  and use it in quickview mix


def channel_safe_loader(path, sr=None, mono=True, offset=0.0, duration=None):
    src, sr = librosa.load(path, sr=sr, mono=mono, offset=offset, duration=duration)
    return np.atleast_2d(src), sr


def channel_safe_writer(path, y, sr, norm=False):
    if y.ndim == 1:  # (n_sample, )
        librosa.output.write_wav(path, y, sr, norm=norm)
    elif y.ndim == 2:
        if y.shape[0] == 1:  # (1, n_sample)
            librosa.output.write_wav(path, y[0], sr, norm=norm)
        else:  # (2, n_sample)
            librosa.output.write_wav(path, y, sr, norm=norm)


def assert_key(keys, a_dict, attr_name):
    for key in keys:
        assert key in a_dict, 'Key {} is required in {}'.format(key, attr_name)


def assert_positive(attr, attr_name):
    assert attr > 0, '{} should be > 0, but it is {}'.format(attr_name, attr)


def find_peak_energy(src, nsp_frame, nsp_hop):
    '''Sweeps with a frame,
    returns the start and end sample index
    src: 1d array (mono)
    nsp_frame: integer, length of frame [sample]
    nsp_hop: integer, length of hop [sample]

    return: a tuple, (start_index, duration) (both in [sample])
    '''
    assert len(src) != 0, 'Wrong input in find_peak, source length is 0.'
    if nsp_frame >= len(src):
        return (0, nsp_frame - 1)

    n_frame = (len(src) - nsp_frame + nsp_hop) / nsp_hop

    energies = np.zeros((len(src) / nsp_hop + 1))

    for idx, sp_from in enumerate(range(0, len(src), nsp_hop)):
        energies[idx] = np.sum(src[sp_from: sp_from + nsp_frame] ** 2)

    energies = np.array(energies)
    max_idx = np.argmax(energies)
    offset = max_idx * nsp_hop
    return offset, nsp_frame


def get_normalize_coeff(src):
    '''returns coeff to MULTIPLY to normalize.
    Robust to stereo/mono variance. '''
    return np.min((1. / np.max(np.abs(src)), 4))  # more than x4 is weird.


def event_convert(events, sr, case):
    """
    events: list of event timings
    sr: integer, sampling rate
    case: string, 'sp2sec', 'sec2sp'"""

    def _sp2sec(events, sr):
        return [ev / sr for ev in events]

    def _sec2sp(events, sr):
        return [ev * sr for ev in events]

    sr = float(sr)
    if case == 'sp2sec':
        return _sp2sec(events, sr)
    elif case == 'sec2sp':
        return _sec2sp(events, sr)


def force_folder(folder_path, mode=None):
    absolute = folder_path.startswith('/')

    folders = folder_path.split('/')
    if '' in folders:
        folders.remove('')
    if absolute:
        fullpath = '/'
    else:
        fullpath = ''
    for folder in folders:
        fullpath = os.path.join(fullpath, folder)
        try:
            os.mkdir(fullpath)
        except:
            pass
        if mode is not None:
            try:
                os.chmod(fullpath, mode)
            except:
                pass


def path_iter(path_root):
    path_root = path_root.rstrip('/')
    numbers = [str(i) for i in range(10)]
    alphabets = list(string.ascii_lowercase)

    for sub1 in numbers:
        for sub2 in numbers + alphabets:
            for sub3 in numbers + alphabets:
                yield '%s/%s/%s/%s' % (path_root, sub1, sub2, sub3)


def track_id_to_path(track_id, ext='npy'):
    return '{}/{}/{}/{}.{}'.format(track_id[0],
                                   track_id[1].lower(),
                                   track_id[2].lower(),
                                   track_id,
                                   ext)
