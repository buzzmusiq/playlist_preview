

import json
import timeit

import quickview
from song import Song

import librosa


def main_one_file_path(filepath):
    """filepath: fileapth of audio file (mp3, wav, ...).

    It is used when there's downloaded music file
    """

    t1 = timeit.default_timer()
    qc = quickview.QuickviewConfig(nsp_fft=1024,
                                   nsp_hop=512)
    t2 = timeit.default_timer()
    print (t2-t1)

    song = Song(srcpath=filepath, sr=qc.sr)
    t3 = timeit.default_timer()
    print (t3-t2)

    thumbnail, midi_like_info = song.do_all_in_one(qc.nsp_chorus, qc.nsp_chorus_hop, qc.nsp_fft,
                                                   qc.nsp_hop, qc.pp_kick, qc.pp_snare,
                                                   qc.nsp_hop, qc.nsp_hop)
    t4 = timeit.default_timer()
    print (t4-t3)




    testTrack = '3dhjNA0jGA8vHBQ1VdD6vV'
    outThumbnailFilePath = './{}_thumbnail.wav'.format(testTrack)

    librosa.output.write_wav(outThumbnailFilePath,
                                 thumbnail, qc.sr)


    t5 = timeit.default_timer()
    print (t5-t4)                                 

    outJsonFilePath = './{}_thumbnail.json'.format(testTrack)                                
    with open(outJsonFilePath, 'w') as f_out:
            json.dump(midi_like_info, f_out)
    t6 = timeit.default_timer()
    print (t6-t5)                    

    print (t6-t1)
    print (midi_like_info)                                                   
    print (thumbnail)


                                    






def lambda_handler(event, context):
    testTrack = '3dhjNA0jGA8vHBQ1VdD6vV'
    mp3FileFile = './{}.mp3'.format(testTrack)

    main_one_file_path(mp3FileFile)


if __name__ == '__main__':
    testTrack = '3dhjNA0jGA8vHBQ1VdD6vV'
    mp3FileFile = './{}.mp3'.format(testTrack)

    main_one_file_path(mp3FileFile)