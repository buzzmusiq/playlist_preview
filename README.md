# Playlist preview

## 의존성
 * `pip install numpy`
 * `pip install librosa==0.5` (`Mac에서 에러 발생시 : sudo pip install librosa --ignore-installed`)


## 사용 방법

```
song_config = SongConfig()
song = Song(song_config, srcpath="previews/6FIRmGJ5kyfTTP0MtOs6cl.mp3")
song.do_all_in_one()

song2 = Song2(song_config, srcpath="previews/song2.m4a")
song2.do_all_in_one()
```

## Data version
 * version 1.0: 2017-11-20.
```python
{
    'version': VERSION,
    'bpm': self.tempo,
    'energy': self.waveform,
    'kick_velocity': self.vel_kick,
    'snare_velocity': self.vel_snare,
    'kick_timing_s': utils.event_convert(self.onsets_kick_sp, self.sr, 'sp2sec'),  # unit: [second]
    'snare_timing_s': utils.event_convert(self.onsets_snare_sp, self.sr, 'sp2sec'),  # unit: [second]
}
 ```

### 사진간 경계선 사용법
 * 추가 2017-10-09
 * 목적 : 사진이 여러 장일 때 온셋에 맞춰서 적당한 경계선을 찾아준다
 * 사용법
   - `song.do_all_in_one()`을 실행하면 `song.optimal_boundaries`가 채워짐
   - 얘는 list of list고, `len(song.optimal_boundaries)`는 항상 6임
     - 더 정확히는 `MAX_N_SEG+1`. `MAX_N_SEG`는 파일 상단에서 5로 정의
   - 예를 들면 아래와 같은 값이 나옴
     - `[[], [], [[13.978412698412699]], [[9.218321995464853, 19.71374149659864]], [[6.977596371882086, 14.97687074829932, 22.221496598639455]], [[5.979138321995465, 11.226848072562358, 17.96063492063492, 23.962993197278912]]]`
   - 만일 사진이 3장이라면 경계가 2개 필요함 이 값은
     - `song.optimal_boundaries[3]`의 값인 `[9.218321995464853, 19.71374149659864]`를 사용하면 됨.
     - 단위는 [초].
