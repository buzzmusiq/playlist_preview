import librosa
import numpy as np
from librosa.util import normalize
from librosa import stft, istft, frames_to_samples, frames_to_time, samples_to_time
from librosa.onset import onset_detect
from librosa.onset import onset_strength as compute_onset_strength
from librosa.beat import estimate_tempo, beat_track
from madmom.audio.signal import load_audio_file

VERSION = '1.2.3'  # 2018-01-12 # fix zero-tempo cases


class SongConfig():
    def __init__(self, nsp_fft=512, nsp_hop=256, sr=22050, fps=30.):
        """Configuration class for Song

        parameters
        ----------
            nsp_fft: int, number of FFT points, should be 2 ** n
            nsp_hop: int, hop between STFT frames, should be < nsp_fft
            sr: ing, sampling rate.
            fps: float, frame rate for waveform visualisation
        """
        self.sr = sr
        self.nsp_fft = nsp_fft
        self.nsp_hop = nsp_hop  # minimum diameter for between-kicks/snares
        self.pp_kick = {'pre_max': 0.25 * sr // nsp_hop,  # 0.1 --> 100 ms
                        'post_max': 0.25 * sr // nsp_hop,  # reference tempo: 120
                        'pre_avg': 0.125 * sr // nsp_hop,
                        'post_avg': 0.125 * sr // nsp_hop,
                        'wait': 0.125 * sr // nsp_hop,
                        'delta': 0.13}  # x[n] > mean(-0.1 : 0.1) + delta
        self.pp_snare = {'pre_max': 0.15 * sr // nsp_hop,
                         'post_max': 0.15 * sr // nsp_hop,
                         'pre_avg': 0.07 * sr // nsp_hop,
                         'post_avg': 0.07 * sr // nsp_hop,
                         'wait': 0.06 * sr // nsp_hop,
                         'delta': 0.1}

        self.support_exts = ['mp3', 'wav', 'm4a', 'ogg', 'au', 'm4p']

        self.fps = fps  # fps that is applied for energy-per-frame computation
        self.min_velocity = 0.01  # minimum velocity. onsets below this are ignored.
        # HPSS
        self.n_freq_kick = (0, 1 + int(13 * self.nsp_fft / 1024))  # offset and length.
        self.n_freq_snare = (1 + int(30 * self.nsp_fft / 1024), 1 + int(200 * nsp_fft / 1024))  # offset and length.
        # do HPSS.                                            # For harmonic, 21 when 22050 hz,
        median_kernel_h = 2 * int(10 * self.sr / 22050.) + 1  # if Smaller -> bigger H mask --> strict on percussive
        median_kernel_p = 2 * int(15 * self.sr / 22050.) + 1  # 31 when 22050 hz.
        self.hpss_kernel_sizes = (median_kernel_h, median_kernel_p)

        self.min_tempo = 60
        self.max_tempo = 180
        # self.max_tempo MUST BE bigger than 2*self.min_tempo


class Song():
    def __init__(self, song_config, srcpath):
        """Song class for onset and boundary detection.

        parameters
        ----------
            song_config: song_config instance
            srcpath: source path to load.
        """
        # basic metadata
        self.srcpath = srcpath
        self.src_len = None  # [second]
        self.nsp_src = None  # [samples]
        # config
        self.config = song_config
        self.sr = song_config.sr
        # sources
        self.src_mono = None  # np, audio signal. (N, ). E.g., N can be 1214223
        self.SRC_mono, self.SRC_percu, = None, None
        self.src_kick, self.src_snare = None, None  # np array, signal. (N, )
        # features
        self.onsets_kick_sp = None  # np array, onset timing [sample]. (K, ). E.g., K can be 86
        self.onsets_snare_sp = None  # same but for snare
        self.vel_kick, self.vel_snare = None, None  # np array, onset strength in [0, 1]. (K, )
        self.waveform = None  # np array, waveform for visualisation. (900, ) if 30 fps.
        self.tempo = None  # [bpm]
        self.onset_strength = None  #
        self.beats = None  # np array [second]

    def do_all_in_one(self):
        """Run all the function to complete the class instance."""
        # do all the work.
        self.load_audio()
        self.stft()
        self.feat_from_src()
        self.feat_from_SRC()
        self.feat_from_hpss()
        return self.get_event_info()

    def load_audio(self):
        """Load the audio file from self.srcpath and downsample if needed"""
        try:
            self.src_mono, _ = load_audio_file(self.srcpath, self.sr, 1, dtype=np.float_)
            self.src_mono = librosa.util.normalize(self.src_mono).astype(np.float_)
            self.nsp_src = len(self.src_mono)
            self.src_len = float(self.nsp_src) / float(self.sr)
        except IOError as Error:
            raise IOError('librosa.load error, no such file at {}\n{}'.format(self.srcpath, repr(Error)))

    def stft(self):
        """Get STFT"""
        self.SRC_mono = stft(self.src_mono, n_fft=self.config.nsp_fft,
                             hop_length=self.config.nsp_hop)

    def feat_from_src(self):
        """[1/3] Those requires self.sef_mono"""
        self.waveform = self.get_waveform()

    def feat_from_SRC(self):
        """[2/3] Those requires self.SRC_mono. It takes most of the time.
        """
        self.onset_strength = compute_onset_strength(None, sr=self.sr, S=np.abs(self.SRC_mono),
                                                     aggregate=np.median, hop_length=self.config.nsp_hop)
        self.tempo, self.beats = self.get_beat_track(self.onset_strength)
        self.SRC_percu = self.hpss()

    def feat_from_hpss(self):
        """[3/3] Those requires self.SRC_percu"""
        self.src_kick, self.onsets_kick_sp = self.get_onsets(self.config.n_freq_kick,
                                                             self.config.pp_kick, 'kick')
        self.src_snare, self.onsets_snare_sp = self.get_onsets(self.config.n_freq_snare,
                                                               self.config.pp_snare, 'snare')
        self.vel_kick = self.get_vels(self.src_kick, self.onsets_kick_sp)
        self.vel_snare = self.get_vels(self.src_snare, self.onsets_snare_sp)

    def get_waveform(self):
        """Compute energy-per-frame where frame: video frame specified by self.fps"""
        frame_nsp = int(self.sr / float(self.config.fps))
        y_frames = librosa.util.frame(self.src_mono, frame_length=frame_nsp, hop_length=frame_nsp)  # (512, N_frame)
        energies = np.max(y_frames ** 2, axis=0)  # (N_frame, )
        if np.max(energies) == 0:
            return np.zeros((len(energies),))  # when signal is all zeros
        energies /= np.max(energies)  # normalize it
        return energies ** 0.4  # some tweaks

    def hpss(self):
        """Do harmonic-percussive separation.
        Returns complex64 spectrograms."""
        hpss_kernel_sizes = self.config.hpss_kernel_sizes
        _, SRC_percu = librosa.decompose.hpss(self.SRC_mono, kernel_size=hpss_kernel_sizes, margin=(1.0, 1.0))
        return SRC_percu

    def get_onsets(self, n_freq, pp, name):
        """n_freq : tuple, num_freq for mask
        pp: peak picking dictionary
        name: str, in ('kick', 'snare')
        """
        mask = np.zeros_like(self.SRC_percu, dtype=np.bool)
        mask[n_freq[0]:n_freq[1], :] = 1
        filtered_SRC = self.SRC_percu * mask
        filtered_src = istft(filtered_SRC, hop_length=self.config.nsp_hop)
        onset_env = compute_onset_strength(y=None, sr=self.sr, S=np.abs(filtered_SRC[n_freq[0]:n_freq[1]]),
                                           aggregate=np.median, hop_length=self.config.nsp_hop)
        onsets_sp = onset_detect(None, self.sr, onset_envelope=onset_env,
                                 hop_length=self.config.nsp_hop,
                                 units='samples', **pp)
        if len(onsets_sp) == 0:
            if name == 'kick':
                onsets_sp = [self.nsp_src / 11 * i for i in range(1, 11, 2)]
            else:
                onsets_sp = [self.nsp_src / 11 * i for i in range(2, 11, 2)]

        return librosa.util.normalize(filtered_src), np.array(onsets_sp)

    def get_beat_track(self, onset_strength):
        """wrapper of librosa beat_track"""
        tempo, beats = beat_track(y=None, sr=self.sr, onset_envelope=onset_strength,
                                  hop_length=self.config.nsp_hop, units='time')
        if tempo <= 1:
            tempo = 60
        while tempo < self.config.min_tempo:
            tempo *= 2
        while tempo > self.config.max_tempo:
            tempo /= 2.
        return tempo, beats

    def get_vels(self, src_ref, onsets):
        """min vel is ignored now. to add it, merge get_vel and get_onsets"""
        nsp_near = self.config.nsp_hop // 2
        vels = np.array([np.max(np.abs(src_ref[np.amax((0, os - nsp_near))
        :np.amin((len(src_ref), os + nsp_near))])) for os in onsets])
        if vels.max() == 0.0:
            vels += 0.1
        return librosa.util.normalize(vels)

    def get_event_info(self):
        """returns a dictionary of each event"""
        return {
            'version': VERSION,
            'bpm': self.tempo,
            'beat': self.beats.tolist(),
            'energy': self.waveform.tolist(),
            'kick_velocity': self.vel_kick.tolist(),
            'snare_velocity': self.vel_snare.tolist(),
            'kick_timing_s': samples_to_time(self.onsets_kick_sp, self.sr).tolist(),  # unit: [second]
            'snare_timing_s': samples_to_time(self.onsets_snare_sp, self.sr).tolist(),  # unit: [second]
        }


if __name__ == "__main__":
    import song_tester
    import sys

    if len(sys.argv) == 1:
        fpath = "3dhjNA0jGA8vHBQ1VdD6vV.mp3"
    else:
        fpath = sys.argv[1]
    print('Testing %s ...' % fpath)
    song_tester.timeit(1, fpath)
    # song_tester.timeit(1, "previews/00_rachel.mp3")
