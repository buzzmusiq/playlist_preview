from song import Song, SongConfig
import numpy as np
import librosa
import utils


class SongDemo(Song):
    """An inherited class just for demo during dev"""

    def draw(self, save_filename=None):
        import matplotlib.pyplot as plt

        n_subplot = 4

        plt.figure(figsize=(15, 3 * n_subplot))
        ax = plt.subplot(n_subplot, 1, 1)
        ax.plot(self.src_kick, color='b', alpha=0.4)
        ax.plot(self.src_snare, color='g', alpha=0.4)

        kicks = zip(self.onsets_kick_sp, self.vel_kick)
        snares = zip(self.onsets_snare_sp, self.vel_snare)

        ax.vlines(self.onsets_kick_sp, 2 * np.ones_like(self.onsets_kick_sp) - self.vel_kick,
                  2, alpha=0.6, color='b', linestyle='-', label='onset(kick)')
        ax.vlines(self.onsets_snare_sp, 2 * np.ones_like(self.onsets_snare_sp) - self.vel_snare,
                  2, alpha=0.6, color='g', linestyle='-', label='onset(snare)')

        if len(self.beats) > 0:
            ax.stem(librosa.time_to_samples(self.beats, self.sr), np.ones_like(self.beats))
            ax.set_ylim([0, 2])
            ax.set_xlabel('time [s]')
            ax.set_title(str(int(self.tempo)) + ' bpm')
            ax.legend(loc=4)

        ax = plt.subplot(n_subplot, 1, 2)
        ax.plot(self.src_kick)
        ax.vlines(self.onsets_kick_sp, 2 * np.ones_like(self.onsets_kick_sp) - self.vel_kick,
                  2, alpha=0.6, color='b', linestyle='-', label='onset(kick)')
        ax.set_ylim([0, 2])
        ax.set_title('kick')

        ax = plt.subplot(n_subplot, 1, 3)
        ax.plot(self.src_snare)
        ax.vlines(self.onsets_snare_sp, 2 * np.ones_like(self.onsets_snare_sp) - self.vel_snare,
                  2, alpha=0.6, color='b', linestyle='-', label='onset(snare)')
        ax.set_ylim([0, 2])
        # ax.set_xticks([i * self.sr for i in xrange(30)], [str(i) for i in xrange(30)])
        ax.set_title('snare')

        plt.tight_layout()
        if save_filename is not None:
            plt.savefig(save_filename)
        plt.close()

    def draw_waveform(self):
        plt.figure(figsize=(20, 4))
        ax = plt.subplot(2, 1, 1)
        ax.plot(self.src_mono)
        ax.set_title('Waveform of the original mono file')
        ax.set_ylim([0, 1])
        ax.set_xlim([0, len(self.src_mono)])

        ax = plt.subplot(2, 1, 2)
        ax.plot(self.waveform)
        ax.set_title('waveform per frame')
        ax.set_ylim([0, 1])
        plt.tight_layout()

        if save_filename is not None:
            plt.savefig(save_filename.replace('.png', '_energy.png'))
        plt.close()


def example():
    song_config = SongConfig()
    song = SongDemo(song_config, srcpath="previews/6FIRmGJ5kyfTTP0MtOs6cl.mp3")
    song.do_all_in_one()
    song.draw('groovo.png')


def dev():
    song_config = SongConfig()
    song = SongDemo(song_config, srcpath="previews/keep_up_short.mp3")
    # song = SongDemo(song_config, srcpath="previews/00_fifth.mp3")
    song.do_all_in_one()
    song.draw('low_amplitude.png')
    events = song.get_event_info()
    print_result(events)
    librosa.output.write_wav('kick.wav', song.src_kick, song.sr)
    librosa.output.write_wav('snare.wav', song.src_snare, song.sr)

    # print('when offset is 0.0sec, the optimal boundaries are..:')
    # print(song.optimal_boundaries)
    # song.get_segments(sec_offset=10., sec_vis_len=15.)  # for example
    # print('\nwhen offset is 10.0 sec:')
    # print(song.optimal_boundaries)
    # song.get_segments(sec_offset=13., sec_vis_len=15.)  # for example
    # print('\nwhen offset is 13.0 sec:')
    # print(song.optimal_boundaries)


def print_result(event_info):
    np.set_printoptions(precision=2)
    print("{} kicks, {} snares".format(len(event_info['kick_velocity']),
                                       len(event_info['snare_velocity'])))
    print('bpm', event_info['bpm'])
    print('energy: ', np.array(event_info['energy'][:5]))
    print('kick_velocity :', np.array(event_info['kick_velocity'][:10]))
    print('snare_velocity:', np.array(event_info['snare_velocity'][:10]))
    print('kick_timing   :', np.array(event_info['kick_timing_s'][:10]))
    print('snare_timing  :', np.array(event_info['snare_timing_s'][:10]))
    print('---')


def timeit(repeat, srcpath="previews/00_fifth.mp3"):
    import time
    song_config = SongConfig()
    song = SongDemo(song_config, srcpath=srcpath)

    stime = time.time()
    # repeat = 100
    for _ in range(repeat):
        event_info = song.do_all_in_one()  # key: bpm, energy, kick_velocity, snare_velocity, kick_timing_s, kick_timing_s
    duration = time.time() - stime
    print_result(event_info)
    print('%4.2f seconds for x %d. %4.3f second per run' % (duration, repeat, duration / float(repeat)))
    song.draw('result' + srcpath.split('/')[-1].split('.')[0] + '.png')


def unittest():
    def test_config(song_config):
        utils.assert_type(int, song_config.sr, 'self.SR', positive=True)
        utils.assert_type(int, song_config.nsp_fft, 'self.nsp_fft', True)
        utils.assert_type(int, song_config.nsp_hop, 'self.nsp_hop', True)
        utils.assert_type(dict, song_config.pp_kick, 'self.pp_kick')
        utils.assert_type(dict, song_config.pp_snare, 'self.pp_snare')
        utils.assert_key(('pre_max', 'post_max', 'pre_avg', 'post_avg', 'wait', 'delta'),
                         song_config.pp_kick, 'self.pp_kick')
        utils.assert_key(('pre_max', 'post_max', 'pre_avg', 'post_avg', 'wait', 'delta'),
                         song_config.pp_snare, 'self.pp_snare')
        utils.assert_type(float, song_config.fps, 'self.fps', positive=True)

    def test_song(song):
        utils.assert_type(np.float_, song.tempo, 'self.tempo', True)
        utils.assert_type(float, song.waveform[0], 'self.waveform[0]', True)
        utils.assert_type(np.float_, song.vel_kick[0], 'self.vel_kick[0]', True)
        utils.assert_type(np.float_, song.vel_snare[0], 'self.vel_snare[0]', True)

        utils.assert_type(np.int_, song.onsets_kick_sp[0], 'self.onsets_kick_sp[0]', True)
        utils.assert_type(np.int_, song.onsets_snare_sp[0], 'self.onsets_snare_sp[0]', True)
        utils.assert_type(np.float_, utils.event_convert(song.onsets_kick_sp, song.sr, 'sp2sec')[0],
                          'converted onsets_kick_s', True)

        utils.assert_type(np.ndarray, song.waveform, 'self.waveform')
        utils.assert_type(np.ndarray, song.vel_kick, 'self.vel_kick')
        utils.assert_type(np.ndarray, song.vel_snare, 'self.vel_snare')
        utils.assert_type(np.ndarray, song.onsets_kick_sp, 'self.onsets_kick_sp')
        utils.assert_type(np.ndarray, song.onsets_snare_sp, 'self.onsets_snare_sp')
        utils.assert_type(np.ndarray, song.onsets_snare_sp, 'self.onsets_snare_sp')

    song_config = SongConfig()
    test_config(song_config)

    song = SongDemo(song_config, srcpath="3dhjNA0jGA8vHBQ1VdD6vV.mp3")
    song.do_all_in_one()
    test_song(song)

    print('Test is done without any error.')


if __name__ == "__main__":
    unittest()
